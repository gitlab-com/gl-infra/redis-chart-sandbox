## Testing Announce Hostnames Change


1. (Re-)create the 3 bench VMs via terraform
1. `scripts/bench-redis-server-configure` - ask those servers to become a
   workable cluster
1. `./redis-validate.sh` - outputs a bunch of diagnostic information - cluster
   should be healthy at this point
1. `scripts/mock_chef_change.sh` - this mimics what needs to be
   accomplished via our chef infra - changes only the `/etc/gitlab/gitlab.rb`
   config file
1. `scripts/bench-redis-server-reconfigure.rb` - this will perform the work
   noted below
      - optionally start `scripts/run-benchmarks` to generate load while doing
        the failover
1. `./redis-validate.sh` - outputs a bunch of diagnostic information - cluster
   should be healthy at this point

### `bench-redis-server-reconfigure.rb`

1. Detects the redis server topology (to an extent)
1. Runs validation prior to reconfiguring the secondary servers
1. fails over the primary
1. stops sentinel on the old primary
1. reconfigures the old primary
1. brings online sentinel

### switching back to announcing IPs

1. determine the current primary (e.g. by running `./redis-validate.sh`)
1. revert the config: `scripts/mock_chef_change.sh --revert <nr_of_primary_node>`
     * The script needs to know which node (0,1 or 2) is the current primary
1. `scripts/bench-redis-server-reconfigure.rb`
