#!/usr/bin/env ruby
# frozen_string_literal: true

require 'open3'
require 'pry'

@project = ENV['TF_VAR_project'] || 'gitlab-scalability-epic-619'
@redis_cluster = ENV['REDIS_CLUSTER_NAME'] || 'gitlab-redis'
region = ENV['TF_VAR_region'] || 'us-east1'
vmname_pattern = ENV['VM_NAME_PATTERN'] || 'bench-redis-c2-vm-X'
start_count = ENV['VM_START_COUNT'].to_i
target_hosts = ENV['TARGET_HOSTS']&.split(',')
@zone = "#{region}-b"

@hosts = []

# populate hosts
counter = start_count
loop do
  @hosts << vmname_pattern.gsub(/X/, counter.to_s)
  counter += 1
  break if counter == start_count + 3
end

@reset_command = <<-COMMAND
  sudo /opt/gitlab/embedded/bin/redis-cli -p 26379 sentinel reset gitlab-redis
COMMAND

@retriable = true
@not_retriable = false
@redis_cli = %{REDISCLI_AUTH=$(sudo grep ^requirepass /var/opt/gitlab/redis/redis.conf|cut -d\" \" -f2 | tr -d \\") /opt/gitlab/embedded/bin/redis-cli --raw}
@sentinel_cli = %{/opt/gitlab/embedded/bin/redis-cli --raw -p 26379}.gsub('"', ("\\\"")).gsub('$', '\$')
@primary = []
@primary_ip = []
@secondaries = []
@secondary_ips = []

def confirm_service_restart(host, service)
  count = 0
  command = "ps -o etimes -p $(pgrep #{service}) --no-headers"
  loop do
    stdout = run(host, command, @retriable)
    if stdout.to_i <= 120
      puts "    > confirmed #{service} was restarted"
      break
    else
      count += 1
      break if count >= 5
      raise "Suspected that #{service} did not restart, detected uptime of process: #{stdout.chomp}"

      sleep 10
    end
  end
end

def ssh(host, command)
  if ENV['USE_SSH'] == 'true'
    "ssh #{host}.#{ENV['VM_DNS_ZONE']} \'#{command}\'"
  else
    "gcloud compute ssh #{host} --project=#{@project} --zone=#{@zone} -- \'#{command}\'"
  end
end

def run(host, command, allow_retry)
  attempts = allow_retry ? 0 : 99
  loop do
    stdout, stderr, status = Open3.capture3(ssh(host, command))

    if status.exitstatus == 0
      return stdout
    elsif attempts <= 2
      attempts += 1
      puts "retrying last command: #{command}; on host #{host}..."
      sleep 5
    elsif attempts > 2
      raise "Status is non-zero: #{status}; #{stderr}"
    end
  end
end

def detect_topology
  puts '> detecting primary...'
  @hosts.each do |host|
    stdout = run(host, "#{@redis_cli} role", @retriable)

    if stdout.split[0].chomp == "master"
      @primary += [host]
      stdout = run(host, 'hostname -i', @retriable)
      @primary_ip = [stdout.chomp]
    else
      @secondaries += [host]
      stdout = run(host, 'hostname -i', @retriable)
      @secondary_ips += [stdout.chomp]
    end
  end

  if @primary.length > 1
    raise 'More than one primary detected! Bailing...'
  end

  puts "> script is configured:"
  puts "    primary: #{@primary} - #{@primary_ip}"
  puts "    secondaries: #{@secondaries} - #{@secondary_ips}"
end

def reconfigure_primary
  puts '> reconfiguring the primary...'
  host = @primary[0]
  stdout = run(host, "#{@redis_cli} role", @retriable)

  if stdout.split[0].chomp == "master" then
    puts "    role is: #{stdout.split[0].chomp}, failing over..."
    stdout = run(@secondaries[0], "#{@sentinel_cli} sentinel failover #{@redis_cluster}", @not_retriable)
    puts "    > failover request result: #{stdout}"
  else
    raise "Role was detected incorrectly: #{stdout}, bailing..."
  end

  # make sure we've got a new primary
  puts '    > detecting sucessful failover...'
  stdout = run(@secondaries[0], "#{@sentinel_cli} sentinel masters", @retriable)
  if stdout.split[3].chomp.match?(/#{@primary[0]}.*/) && stdout.split[3].chomp.match?(@primary_ip[0])
    raise 'Failover did not occur! Bailing...'
  end

  print '    > waiting for completed stepdown...'
  count = 0
  loop do
    stdout = run(host, "#{@redis_cli} role", @retriable)
    break if stdout.split[0].chomp == "slave"

    break if count >= 30
    print '.'
    sleep 5
  end
  puts

  validate_redis_ok(host)

  # Stopping sentinel to avoid sending bad data to clients
  puts '    > stopping sentinel...'
  run(host, 'sudo gitlab-ctl stop sentinel', @retriable)
  # getting the status of service will have a $?=3 :unamused:
  stdout = run(host, 'sudo gitlab-ctl status sentinel; /bin/true', @retriable)
  if stdout.split[0].chomp != 'down:'
    raise 'Sentinel may not be down, which means clients will receive incorrect information, Bailing!...'
  end
  puts "< failover_if_master #{host}"
  reconfigure_secondaries([host])
end

def reconfigure_secondaries(hosts)
  hosts.each do |host|
    validate_redis_ok(host)
    reconfigure(host)

    puts '> waiting for 30 seconds to allow sentinels to converge...'
    sleep 30

    puts '> resetting sentinel replicas...'

    @hosts.each do |host|
      stdout = run(host, @reset_command, @retriable)

      puts "    > reset #{host} result: #{stdout}"
    end

    puts '    > waiting for 30 seconds per documentation...'
    sleep 30

    puts '< resetting sentinel replicas'
  end
end

def validate_redis_ok(host)
  puts "> validating that #{host} is okay..."

  count = 0
  loop do
    count += 1

    stdout = run(host, "#{@redis_cli} role", @retriable)

    role = stdout.split[0].chomp
    redis_status = stdout.split[3]&.chomp

    if role == "master"
      puts "    role is: #{role}, waiting for host to become replica..."
      sleep 10
      next
    end

    if role == "slave" then
      puts "    role is: #{role}, we're okay, moving on ..."
    else
      raise "Unexpected role: #{role}, exiting..."
    end

    if redis_status == "connected" then
      puts "    replica is: #{redis_status}, we're okay, moving on ..."
      break
    elsif redis_status == "sync" then
      puts "    replica is: syncing, let's wait for sync to finish..."
      sleep 5
      next
    else
      puts "    Unexpected status: #{redis_status}: (#{stdout}), retrying..."

      if count > 10 then
        raise "Giving up!"
      end

      sleep 5
      next
    end
  end

  puts "    host #{host} is indeed okay, moving on..."
end

def failover_if_master(host)
  puts "> failover_if_master #{host}"
  stdout = run("#{@redis_cli} role")

  role = stdout.split[0].chomp

  # if role is master, perform failover
  if role == "master" then
    puts "> role is: #{role}, failing over..."
    run(host, "#{@sentinel_cli} sentinel failover #{@redis_cluster}", @not_retriable)
  elsif role == "slave"
    puts "> role is: #{role}, NOT failing over, moving on ..."
  else
    raise "Role was detected incorrectly: #{stdout}"
  end

  # wait for master to step down and sync (expect "slave" [sic] and "connected")
  count = 0
  loop do
    count += 1
    stdout = run(host, "#{@redis_cli} role", @retriable)

    role = stdout.split[0].chomp

    break if role == 'slave'

    if count >= 3
      puts "Failed to step down: #{stdout}"
      raise
    end

    puts '< waiting for stepdown'
    sleep 30
  end

  loop do
    stdout = run(host, "#{@redis_cli} role", @retriable)

    redis_status = stdout.split[3].chomp

    break if redis_status == 'connected'

    puts '< waiting for sync'
    sleep 30
  end

  stdout = run(host, "hostname; echo; hostname -i", @retriable)

  hostname = stdout.split[0].chomp
  ip = stdout.split[1].chomp

  ## wait for sentinel to ack the master change
  loop do
    stdout = run(host, "#{@sentinel_cli} sentinel master #{@redis_cluster}", @retriable)

    redis_status = stdout.split[3].chomp
    break if hostname != redis_status && ip != redis_status

    puts '< waiting for sentinel'
    sleep 1
  end

  puts "< failover_if_master #{host}"
end

def reconfigure(host)
  puts "> reconfigure #{host}"

  if ENV['RUN_CHEF_CLIENT'] == 'true'
    puts '    running chef-client...'
    run(host, 'sudo chef-client', @not_retriable)
  end

  puts '    disable rdb for fast restart...'
  run(host, "#{@redis_cli} config set save ''", @not_retriable)
  stdout = run(host, "#{@redis_cli} config get save", @retriable)

  puts '    running gitlab-ctl reconfigure...'
  run(host, 'sudo gitlab-ctl reconfigure', @not_retriable)

  puts '    > forcing sentinel restart...'
  run(host, 'sudo gitlab-ctl restart sentinel', @not_retriable)

  puts '    checking service restarts...'
  confirm_service_restart(host, 'redis-server')
  confirm_service_restart(host, 'redis-sentinel')

  validate_redis_ok(host)

  puts "< reconfigure #{host}"
end

### MAIN

detect_topology
puts

if target_hosts && target_hosts.size > 0
  reconfigure_secondaries(target_hosts)
  puts
  exit
end

reconfigure_secondaries(@secondaries)
puts

reconfigure_primary
