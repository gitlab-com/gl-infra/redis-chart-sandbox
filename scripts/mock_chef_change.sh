#!/bin/bash

set -euo pipefail
set -x

PROJECT="${TF_VAR_project:-gitlab-scalability-epic-619}"
REGION="${TF_VAR_region:-us-east1}"

DNS_ZONE="${TF_VAR_dns_zone:-redis-sandbox.internal}"
[ "$VM_NAME_PATTERN" ] || VM_NAME_PATTERN="bench-redis-c2-vm-X"

primary="0"
[ "$2" ] && primary="$2"

run() {
  # $1: number of VM
  # $2: command to execute

  vmname="${VM_NAME_PATTERN/X/$1}"

  if [ "$USE_SSH" == "false" ]; then
    gcloud compute ssh --project $PROJECT $vmname -- "$2"
  else
    ssh "${vmname}.${VM_DNS_ZONE}" "$2"
  fi
}

primary_name="${VM_NAME_PATTERN/X/$primary}"
primary_fqdn="${primary_name}.${DNS_ZONE}"
primary_ip=$(gcloud compute instances describe ${primary_name} --project=$PROJECT --format='get(networkInterfaces[0].networkIP)')

mock_chef_change=$(cat <<COMMAND
  sudo sh -c "sed -e 's/.*use_hostnames.*//g' -i /etc/gitlab/gitlab.rb"
  sudo sh -c "sed -e 's/.*master_ip.*//g' -i /etc/gitlab/gitlab.rb"
  sudo sh -c "echo 'redis[\"master_ip\"] = \"${primary_fqdn}\"' >> /etc/gitlab/gitlab.rb"
  sudo sh -c "sed -e 's/#\(.*redis.*announce_ip.*bench.*\)/\1/g' -i /etc/gitlab/gitlab.rb"
COMMAND
)

# return config back to announcing IPs
mock_chef_revert=$(cat <<COMMAND
  sudo sh -c "sed -e 's/.*master_ip.*//g' -i'' /etc/gitlab/gitlab.rb"
  sudo sh -c "echo 'redis[\"master_ip\"] = \"${primary_ip}\"' >> /etc/gitlab/gitlab.rb"
COMMAND
)

revert_chef_change() {
  for i in $( seq ${VM_START_COUNT} $(( ${VM_START_COUNT} + 2 )) ); do
    run $i "sudo cp /etc/gitlab/gitlab.rb.pre /etc/gitlab/gitlab.rb"
    run $i "${mock_chef_revert}"
  done
}

if [ "$1" == "--revert" ]; then
  revert_chef_change
  exit 0
fi

for i in $( seq ${VM_START_COUNT} $(( ${VM_START_COUNT} + 2 )) ); do
  run $i "sudo cp /etc/gitlab/gitlab.rb /etc/gitlab/gitlab.rb.pre"
done

for i in $( seq ${VM_START_COUNT} $(( ${VM_START_COUNT} + 2 )) ); do
  run $i "${mock_chef_change}"
done

for i in $( seq ${VM_START_COUNT} $(( ${VM_START_COUNT} + 2 )) ); do
  run $i "sudo diff -U 2 /etc/gitlab/gitlab.rb.pre /etc/gitlab/gitlab.rb" || true
done
