#!/bin/bash
# shellcheck disable=SC2089,SC2016,SC2155,SC2162,SC2029,SC2086,SC2090

set -euo pipefail

PROJECT="${TF_VAR_project:-gitlab-scalability-epic-619}"
REGION="${TF_VAR_region:-us-east1}"

[ "$VM_NAME_PATTERN" ] || VM_NAME_PATTERN="bench-redis-c2-vm-X"
vmcounter=$VM_START_COUNT

export redis_cli='REDISCLI_AUTH="$(sudo grep ^requirepass /var/opt/gitlab/redis/redis.conf|cut -d" " -f2|tr -d \")" /opt/gitlab/embedded/bin/redis-cli --raw'
export sentinel_cli='/opt/gitlab/embedded/bin/redis-cli -p 26379 --raw'

get_info() {
  i=$1
  fqdn="${VM_NAME_PATTERN/X/$i}"

  if [ "$USE_SSH" == "true" ]; then 
    fqdn="${VM_NAME_PATTERN/X/$i}.${VM_DNS_ZONE}"
  fi

  echo "################################################################################"
  echo "Host: ${fqdn}"

  run_cmd="ssh $fqdn"
  if [ "$USE_SSH" == "false" ]; then 
    run_cmd="gcloud compute ssh --project $PROJECT $fqdn --"
  fi

  role=$($run_cmd "$redis_cli role")
  redis_file=$($run_cmd "sudo cat /var/opt/gitlab/redis/redis.conf")
  primary=$($run_cmd "/opt/gitlab/embedded/bin/redis-cli --raw -p 26379 sentinel masters")
  sentinel_file=$($run_cmd "sudo cat /var/opt/gitlab/sentinel/sentinel.conf")
  sentinel_replicas=$($run_cmd "$sentinel_cli sentinel replicas ${REDIS_CLUSTER_NAME}")

  redis_role=$(echo "${role}" | head -n1)
  redis_connection=$(echo "${role}" | awk '{if(NR==2) print $0}')
  redis_connected=$(echo "${role}" | awk '{if(NR==4) print $0}')
  redis_announce_ip=$(echo "${redis_file}" | grep replica-announce-ip)

  echo "Redis Information:"
  if [[ "${redis_role}" =~ .*master.* ]]; then
    echo "> role: ${redis_role}"
    echo "> Known Secondaries:"
    echo "${role}" | awk '{if(NR==3) print $0}'
    echo "${role}" | awk '{if(NR==6) print $0}'
  else
    echo "> role: ${redis_role}"
    echo "> connected to primary: ${redis_connection}"
    echo "> ${redis_connected}"
  fi

  echo "Redis config file stats"
  echo "> redis announce ip: ${redis_announce_ip}"

  echo
  echo "Sentinel Information:"
  known_primary=$(echo "${primary}" | awk '{if(NR==4) print $0}')
  known_role=$(echo "${primary}" | awk '{if(NR==26) print $0}')
  known_replicas_count=$(echo "${primary}" | awk '{if(NR==32) print $0}')
  known_sentinels_count=$(echo "${primary}" | awk '{if(NR==34) print $0}')
  known_quorum_count=$(echo "${primary}" | awk '{if(NR==36) print $0}')
  sentinel_announce_ip=$(echo "${sentinel_file}" | grep 'sentinel announce-ip')
  sentinel_resolve_hostnames=$(echo "${sentinel_file}" | grep 'sentinel resolve-hostnames' || echo "no")
  sentinel_announce_hostnames=$(echo "${sentinel_file}" | grep 'sentinel announce-hostnames' || echo "no")
  sentinel_replica_nodes_a=$(echo "${sentinel_replicas}" | awk '{if(NR==2) print $0}')
  sentinel_replica_nodes_b=$(echo "${sentinel_replicas}" | awk '{if(NR==44) print $0}')

  echo "> sentinel indicated primary: ${known_primary}"
  echo "> sentinel indicated role: ${known_role}"
  echo "> replica count: ${known_replicas_count}"
  echo "> sentinel count: ${known_sentinels_count}"
  echo "> quorum count: ${known_quorum_count}"
  echo "> sentinel replicas:"
  echo "    - ${sentinel_replica_nodes_a}"
  echo "    - ${sentinel_replica_nodes_b}"

  echo "Sentinel config file stats"
  echo "> ${sentinel_announce_ip}"
  echo "> sentinel_resolve_hostnames:  ${sentinel_resolve_hostnames}"
  echo "> sentinel_announce_hostnames: ${sentinel_announce_hostnames}"
}

get_info $vmcounter
vmcounter=$(( $vmcounter + 1 ))
echo

get_info $vmcounter
vmcounter=$(( $vmcounter + 1 ))
echo

get_info $vmcounter
vmcounter=$(( $vmcounter + 1 ))

