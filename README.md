# redis-chart-sandbox

## General setup

```
asdf install
helm repo add bitnami https://charts.bitnami.com/bitnami
helm plugin install https://github.com/databus23/helm-diff
```

## Terraform setup

```shell
# copy .env.example and edit to add PAT with "api" scope as password
source .env
cd terraform
terraform init
terraform plan
terraform apply

# replace 'default' with 'bench' to target the bench cluster
gcloud container clusters get-credentials redis-chart-sandbox-default --region $TF_VAR_region --project $TF_VAR_project

kubectl config use-context gke_${TF_VAR_project}_${TF_VAR_region}_redis-chart-sandbox-default
```

## Deploy external DNS

```
cd externalDNS
./install.sh
```

## Run test scenario (redis install and incremental version upgrade)

```
./test.sh
```

## Configure benchmark Redis servers

After running `terraform apply` (and waiting for the GitLab package
install as part of VM creation):

```
./scripts/bench-redis-server-configure
```

## Enable hostnames in sentinel

See [ENABLE_HOSTNAMES.md](ENABLE_HOSTNAMES.md).

