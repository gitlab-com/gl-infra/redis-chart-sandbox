#!/bin/bash

# minikube start
# kubectx minikube
# stern redis

set -eo pipefail
#set -x

log_grey () {
    echo $(tput bold)$(tput setaf 0)${@}$(tput sgr 0)
}
log_norm () {
    echo $(tput bold)$(tput setaf 9)${@}$(tput sgr 0)
}
log_info () {
    echo $(tput bold)$(tput setaf 4)${@}$(tput sgr 0)
}
log_warn () {
    echo $(tput bold)$(tput setaf 2)${@}$(tput sgr 0)
}
log_err ()  {
    echo $(tput bold)$(tput setaf 1)${@}$(tput sgr 0)
}

wait_for_input() {
  declare input
  while read -n 1 -p "Continue (y/n): " input && [[ $input != "y" && $input != "n" ]]; do
    echo
  done
  if [[ $input != "y" ]]; then
    echo
    echo >&2 "error: aborting"
    exit 1
  fi
  echo
}

# ---

#chart_name=/Users/igor/code/bitnami-charts/bitnami/redis
chart_name=bitnami/redis
chart_version=15.5.2

kubectl delete secret my-release-redis || true
kubectl create secret generic my-release-redis --from-literal=redis-password=hunter5

log_info "uninstall release if exists"
wait_for_input

helm uninstall my-release || true

# ---

echo
log_info "install (dry-run)"
wait_for_input

image=6.2.5-debian-10-r69

helm install my-release $chart_name \
  --dry-run \
  --version $chart_version \
  --set image.tag=$image \
  --set sentinel.image.tag=$image \
  -f values.yaml

echo
log_info "install"
wait_for_input

helm install my-release $chart_name \
  --version $chart_version \
  --set image.tag=$image \
  --set sentinel.image.tag=$image \
  -f values.yaml

echo
log_norm "waiting for replicas to come online..."

while ! [[ "$(kubectl get sts -o json | jq -r '.items[]|(.spec.replicas == .status.readyReplicas)')" = 'true' ]]; do
  sleep 1
done

# ---

image=6.2.6-debian-10-r14

log_info "upgrading image to $image"

for partition in 2 1 0; do
  echo
  log_info "upgrade partition $partition (diff)"
  wait_for_input

  helm diff upgrade my-release $chart_name \
    --version $chart_version \
    --set image.tag=$image \
    --set sentinel.image.tag=$image \
    --set replica.updateStrategy.rollingUpdate.partition=$partition \
    -f values.yaml

  echo
  log_info "upgrade partition $partition"
  wait_for_input

  helm upgrade my-release $chart_name \
    --version $chart_version \
    --set image.tag=$image \
    --set sentinel.image.tag=$image \
    --set replica.updateStrategy.rollingUpdate.partition=$partition \
    -f values.yaml

  sleep 5

  echo
  log_info "waiting for replicas to come online..."

  while ! [[ "$(kubectl get sts -o json | jq -r '.items[]|(.spec.replicas == .status.readyReplicas)')" = 'true' ]]; do
    sleep 1
  done
done

# ---

# helm uninstall my-release
