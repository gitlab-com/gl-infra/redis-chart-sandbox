terraform {
  backend "http" {
  }
}

provider "google" {
  project = var.project
  region  = var.region
}

# default cluster

resource "google_service_account" "default" {
  account_id   = "redis-chart-sandbox-default"
  display_name = "redis-chart-sandbox-default"
}

resource "google_service_account" "dns-admin" {
  account_id   = "dns-admin"
  display_name = "dns-admin"
}

resource "google_project_iam_member" "dns-admin" {
  project = var.project
  role    = "roles/dns.admin"
  member  = "serviceAccount:${google_service_account.dns-admin.email}"
}

resource "google_project_iam_member" "dns-admin-workload-id" {
  project = var.project
  role    = "roles/iam.workloadIdentityUser"
  member  = "serviceAccount:${var.project}.svc.id.goog[default/dns-external-dns]"
}

resource "google_compute_router" "router" {
  name    = "router"
  network = "default"
}

resource "google_compute_router_nat" "nat" {
  name                               = "nat"
  router                             = google_compute_router.router.name
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
}


# default cluster

resource "google_container_cluster" "default" {
  count    = var.create_default_cluster ? 1 : 0
  name     = "redis-chart-sandbox-default"
  location = var.region

  remove_default_node_pool = true
  initial_node_count       = 1

  private_cluster_config {
    enable_private_nodes    = true
    enable_private_endpoint = false
    master_ipv4_cidr_block  = "172.16.0.0/28"
  }

  # needed to enable ip_aliases for a VPC-native cluster
  ip_allocation_policy {
    cluster_ipv4_cidr_block = ""
    services_ipv4_cidr_block = ""
  }

  workload_identity_config {
    workload_pool = "${var.project}.svc.id.goog"
  }
}

resource "google_container_node_pool" "default_redis_c2" {
  count      = var.create_default_cluster ? 1 : 0
  name       = "redis-c2"
  location   = var.region
  cluster    = google_container_cluster.default[0].name
  node_count = 1

  node_config {
    machine_type = var.default_machine_type

    service_account = google_service_account.default.email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]

    workload_metadata_config {
      mode = "GKE_METADATA"
    }
  }
}

# bench cluster

resource "google_service_account" "bench" {
  count        = var.create_bench_cluster ? 1 : 0
  account_id   = "redis-chart-sandbox-bench"
  display_name = "redis-chart-sandbox-bench"
}

resource "google_container_cluster" "bench" {
  count    = var.create_bench_cluster ? 1 : 0
  name     = "redis-chart-sandbox-bench"
  location = var.region

  remove_default_node_pool = true
  initial_node_count       = 3

  workload_identity_config {
    workload_pool = "${var.project}.svc.id.goog"
  }
}

resource "google_container_node_pool" "bench_redis_c2" {
  count      = var.create_bench_cluster ? 1 : 0
  name       = "redis-c2"
  location   = var.region
  cluster    = google_container_cluster.bench[0].name
  node_count = 3

  node_config {
    machine_type = var.bench_machine_type

    service_account = google_service_account.bench[0].email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]

    workload_metadata_config {
      mode = "GKE_METADATA"
    }
  }
}

resource "google_compute_instance" "bench_redis_c2_vm" {
  count        = var.bench_vm_count
  name         = "bench-redis-c2-vm-${count.index}"
  machine_type = var.bench_machine_type
  zone         = "${var.region}-b"

  boot_disk {
    initialize_params {
      image = var.boot_image
      size  = 50
    }
  }

  scratch_disk {
    interface = "SCSI"
  }

  network_interface {
    network = "default"

    access_config {
    }
  }

  metadata_startup_script = file("${path.module}/../scripts/bench-redis-server-init")

  tags = ["redis"]
}

resource "google_dns_managed_zone" "bench" {
  name     = "bench-zone"
  dns_name = "${var.dns_zone}."
}

resource "google_dns_record_set" "redis" {
  name  = "bench-redis-c2-vm-${count.index}.${google_dns_managed_zone.bench.dns_name}"
  type  = "A"
  ttl   = 30
  count = 3

  managed_zone = "${google_dns_managed_zone.bench.name}"

  rrdatas = [google_compute_instance.bench_redis_c2_vm[count.index].network_interface[0].network_ip]
}

resource "google_compute_firewall" "allow_redis" {
  name    = "allow-redis"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["6379", "26379"]
  }

  target_tags   = ["redis"]
  source_ranges = ["0.0.0.0/0"]
}
