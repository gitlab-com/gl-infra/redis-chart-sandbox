variable "project" {
  type    = string
  default = "gitlab-scalability-epic-619"
}

variable "region" {
  type    = string
  default = "us-east1"
}

variable "dns_zone" {
  type    = string
  default = "redis-sandbox.internal"
}

variable "create_default_cluster" {
  type    = bool
  default = true
}

variable "create_bench_cluster" {
  type    = bool
  default = true
}

variable "bench_vm_count" {
  type    = number
  default = 3
}

variable "node_pool_name" {
  type    = string
  default = "redis-c2"
}

variable "default_machine_type" {
  type    = string
  default = "c2-standard-8"
}

variable "bench_machine_type" {
  type    = string
  default = "c2-standard-8"
}

variable "boot_image" {
  type    = string
  default = "projects/ubuntu-os-cloud/global/images/ubuntu-2004-focal-v20210927"
}

