#!/bin/bash

helm upgrade dns bitnami/external-dns --install --values values.yaml \
  --set serviceAccount.annotations."iam\.gke\.io/gcp-service-account"="dns-admin@${TF_VAR_project}.iam.gserviceaccount.com" \
  --set domainFilters="{${TF_VAR_dns_zone}}"
